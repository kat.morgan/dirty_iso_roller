#!/bin/bash
# Nvidia 390 Offline Driver Install Script
# Version: !DEMO ONLY!  
# Create un-attended install tarball on a clean OS of the intended version with no other modifications
#
# Usage: 
#        ./nvidia-offline-install.sh prep 
#        ./nvidia-offline-install.sh 

set -x

# Set Packages
PACKAGES="nvidia-390"

# Set Run Directory
run_DIR="/root/offline-install"
url_LIST="$run_DIR/apt-offline-download.list"

# Check Run Mode
run_PREP="false"
run_MODE="$1"
[[ $run_MODE != "prep" ]] || run_PREP="true"

# Download the packages for offline install
offline_prep () {
  
  # Create Directory
  mkdir -p $run_DIR/debs && cd $run_DIR

  # Add Repo for graphics packages
  add-apt-repository ppa:graphics-drivers -y && apt update

  # Generate Package Download & Dependency URL List
  sudo apt-get --print-uris --yes --reinstall install $PACKAGES | grep ^\' | cut -d\' -f2 > $url_LIST

  # Download packages
  wget --input-file $url_LIST -P $run_DIR/debs/

  # Tar Install Bundle
  cd $run_DIR
  tar Jcvf offline-install-$PACKAGES.tar.xz .

}

offline_install () {

  # Install Packages
  apt-get install -y $run_DIR/debs/*.deb

  # log to latecmd.log
  echo "offline-nvidia install run once @ date -R"

  # reboot
  shutdown -r

}

if [[ $run_PREP == "true" ]]; then
    echo "Preparing the following for offline installation: $PACKAGES"
    offline_prep
    exit 0
elif [[ $run_PREP != "true" ]]; then
    echo "Installing the following from local file: $PACKAGES"
    offline_install
fi
