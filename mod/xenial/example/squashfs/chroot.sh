#!/bin/bash

mount -t /proc root/proc
mount -t /sys root/sys
mount -t /dev root/dev
mount -t /dev/pts root/dev/pts
chroot root
